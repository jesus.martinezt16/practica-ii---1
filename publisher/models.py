from django.db import models
from django.conf import settings

# Create your models here.

class Input(models.Model):

	campo_choice = (
        ('CC', 'Ciencias Computacionales'),
        ('CT', 'Ciencias de la Tierra'),
        ('CN', 'Ciencias de la Naturaleza '),
        ('CS', 'Ciencias de Sociales '),
        ('C', 'Ciencias Medicas '),
        
    )

	usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
	autor=models.CharField(max_length=80)
	titulo=models.CharField(max_length=80)
	campo = models.CharField(max_length=2, choices=campo_choice)
	documento=models.FileField(upload_to='files/')
	tag=models.SlugField()



	def __str__(self):
		return self.titulo
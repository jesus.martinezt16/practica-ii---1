from django.shortcuts import render , redirect

# Create your views here.
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

from django.views import generic
from django.urls import reverse_lazy

from .models import Input

#
from .forms import LoginForm


class Create(generic.CreateView):
	template_name = "input.html"
	model = Input
	fields = [
		
		"autor",
		"titulo",
		"campo",
		"documento",
		"tag",
	]
	success_url = reverse_lazy("index_view")


def index (request): 
	
	return render (request,"index.html",{})


#view para registro
def signup(request):

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            login(request, user)
            return redirect("index_view")

        else:
            for msg in form.error_messages:
                print(form.error_messages[msg])

            return render(request = request,
                          template_name = "signup.html",
                          context={"form":form})

    form = UserCreationForm
    return render(request = request,
                  template_name = "signup.html",
                  context={"form":form})




class List(generic.ListView):
	template_name = "list.html"
	model = Input

	

class Delete(generic.DeleteView):
	template_name = "delete.html"
	model = Input
	success_url = reverse_lazy("list_view")

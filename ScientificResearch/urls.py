"""ScientificResearch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from publisher import views
from django.contrib.auth.views import logout_then_login
from django.conf.urls import include

urlpatterns = [


    path('admin/', admin.site.urls),

    path('index/',views.index,name="index_view"),
   
    path('signup/', views.signup, name="signup_view"),

    path('input/', views.Create.as_view(), name="input_view"),

    path('list/', views.List.as_view(), name="list_view"),

    path('cuentas/', include('django.contrib.auth.urls')),

    path('delete/<int:pk>/', views.Delete.as_view(), name="delete_view"),

    
]

